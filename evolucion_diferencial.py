import numpy as np
from numpy.random import randint, uniform

def _init_poblacion(n_population=30, bounds=[(-5,5),(-5,5)]):
    M = np.array([[uniform(low=i[0],high=i[1]) for i in bounds]for _ in range(n_population)])
    return M

def _generate(pob, i):
    ids = np.random.choice(np.setdiff1d(range(pob.shape[0]), i), size=3, replace=False)
    choosen = pob[ids]
    return choosen

def _best(pob, func):
    best = ([], float('inf'))
    i_best = None
    for i, x_r in enumerate(pob):
        eval_aux = func(x_r)
        if eval_aux < best[1]:
            best = (x_r, eval_aux)
            i_best = i
    return best, pob[np.arange(len(pob)) != i_best]


def compute(func, n_population=50, bounds=[(3,15),(3,15)], max_iter = 100, F=0.5, CR = 0.1):
    p = 0
    g = 0
    pob = _init_poblacion(n_population, bounds)
    #print(_best(pob,func)[0])
    while g < max_iter:
        for i, x in enumerate(pob):
            x_r = _generate(pob, i)
            x_r_best, x_r = _best(x_r, func)
            #Esquema de mutación: v_i = x_{r_best} + F(x_r2 - x_r3)
            v_mutante = x_r_best[0] + F * (x_r[0] - x_r[1])
            u_i = np.array([])
            for j,d in enumerate(x):
                j_rand = np.random.randint(len(x))
                if np.random.uniform() <= CR or j == j_rand:
                    if v_mutante[j] < bounds[j][0]:
                        u_i = np.append(u_i,bounds[j][0])
                    elif v_mutante[j] > bounds[j][1]:
                        u_i = np.append(u_i,bounds[j][1])
                    else:
                        u_i = np.append(u_i,v_mutante[j])
                else:
                    u_i = np.append(u_i,x[j])
            if func(u_i) <= func(x):
                pob[i] = u_i
        g += 1
    return _best(pob,func)[0]


