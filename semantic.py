import csv
import re
import numpy as np
import prueba as p

from scipy.stats import pearsonr
from scipy.stats import spearmanr
from scipy.optimize import rosen, differential_evolution


def _scalar(x):
    return x*20.5


def total():
    i = 0
    corrs = []
    while i < 100:
        de, h, pp = np.array(read_data(),dtype=np.float64)
        corr, _ = pearsonr(de,h)
        corrs.append(corr)
        i+=1
    return corrs


def read_data():
    path = './corr.csv'
    pattern = r'.*\(b to a\)'
    prog = re.compile(pattern)
    distances_DE = []
    human_judgment_corr = []
    pp = []
    with open(path, 'r') as f:
        csv_reader = csv.DictReader(f)
        for row in csv_reader:
            #print(row)
            human_judgment = row['miller']
            aux = row['disc(b to a)']
            distancias = {k:row[k] for k in row if prog.match(k)}
            distancias_array = np.array(list(distancias.values()),dtype=np.float64)
            #print(distancias)
            bounds = [(-10,10) for _ in range(len(distancias_array))]
            human_judgment_array = np.zeros(len(distancias_array))
            human_judgment_array[0] = human_judgment
            pp.append(aux)
            human_judgment_corr.append(human_judgment)
            def corr_pearson(x):
                corr, _  = pearsonr(x, distancias_array)
                return corr
            #res = p.compute(corr_pearson, bounds=bounds)
            res = differential_evolution(corr_pearson, bounds=bounds, workers=1)
            #print(res[0],res[1])
            res_cero_one = res.x#res[0]#/20.5
            aux_distances = res_cero_one * (distancias_array)
            #m = max(aux_distances)
            #print(aux_distances)
            idx = (np.abs(aux_distances - float(human_judgment))).argmin()
            m = aux_distances[idx]
            distances_DE.append(m)
            #print(distances_DE)
    return distances_DE,human_judgment_corr,pp


