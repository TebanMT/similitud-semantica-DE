# Evaluación de Similitud Semántica en Métodos Basados en Conocimiento Mediante Evolución Diferencial

La idea básica es utilizar un algoritmo DE para una evaluación de similitud semántica óptima basada en diferentes resultados de similitud. Funciona con los valores de similitud proporcionados por varias métricas basadas en el conocimiento. Todas las métricas contribuyen de manera uniforme a evaluar el grado de similitud semántica entre dos conceptos al inicio de la evolución diferencial. Luego, a cada métrica se le asigna iterativamente un peso específico. Finalmente, la métrica que proporciona los resultados más similares al juicio humano recibe el mayor peso después del proceso de evolución automática, incluida la inicialización, la mutación, el cruce y la selección.

## Estado
### En proceso con articulo (tambien en proceso)
